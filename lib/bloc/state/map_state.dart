import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapState {}

class InitialState extends MapState {}

class LoadTripInfoState extends MapState {
  final LatLng latLng;
  final List<LatLng> route;

  LoadTripInfoState(this.latLng, this.route);
}

class ClearTripInfoState extends MapState {}