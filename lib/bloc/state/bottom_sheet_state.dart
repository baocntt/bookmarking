import 'package:flutter_google_map/model/driver_info.dart';

class BottomSheetState {}

class InitialBSState extends BottomSheetState {}

class LoadingState extends BottomSheetState {}

class LoadedState extends BottomSheetState {
  final DriverInfo driverInfo;

  LoadedState(this.driverInfo);
}
