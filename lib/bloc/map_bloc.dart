import 'package:bloc/bloc.dart';
import 'package:flutter_google_map/model/trip_info_res.dart';
import 'package:flutter_google_map/repository/place_service.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'event/map_event.dart';
import 'state/map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final placeService = PlaceService();

  @override
  MapState get initialState => InitialState();

  @override
  Stream<MapState> mapEventToState(MapEvent event) async* {
    if (event is OnMapClickEvent) {
      final TripInfoRes tripInfoRes = await placeService.getStep(
          event.fromLatLng.latitude,
          event.fromLatLng.longitude,
          event.toLatLng.latitude,
          event.toLatLng.longitude);
      final route = List<LatLng>();
      route.add(event.fromLatLng);
      tripInfoRes.steps.forEach((step) {
        route.add(step.endLocation);
      });
      yield LoadTripInfoState(event.toLatLng, route);
    }

    if (event is ClearTripInfoEvent) {
      yield ClearTripInfoState();
    }
  }
}
