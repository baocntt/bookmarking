import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapEvent {}

class OnMapClickEvent extends MapEvent {
  final LatLng fromLatLng;
  final LatLng toLatLng;

  OnMapClickEvent(this.fromLatLng, this.toLatLng);
}

class ClearTripInfoEvent extends MapEvent {}
