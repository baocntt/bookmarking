import 'package:bloc/bloc.dart';
import 'package:flutter_google_map/bloc/state/bottom_sheet_state.dart';
import 'package:flutter_google_map/model/driver_info.dart';

import 'event/bottom_sheet_event.dart';

class BottomSheetBloc extends Bloc<BottomSheetEvent, BottomSheetState> {
  @override
  // TODO: implement initialState
  BottomSheetState get initialState => InitialBSState();

  @override
  Stream<BottomSheetState> mapEventToState(BottomSheetEvent event) async* {
    if (event is FindDriverEvent) {
      yield LoadingState();
      await Future.delayed(Duration(seconds: 2));
      final DriverInfo driverInfo = DriverInfo(3, 'Nguyen Duy Bao',
          'https://cdn1.iconfinder.com/data/icons/avatar-2-2/512/Taxi_Driver-512.png');
      yield LoadedState(driverInfo);
    }

    if (event is ReplanTripEvent) {
      yield InitialBSState();
    }
  }
}
