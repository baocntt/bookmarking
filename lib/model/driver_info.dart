class DriverInfo {
  final int _rating;
  final String _name;
  final String _avatarUrl;

  DriverInfo(this._rating, this._name, this._avatarUrl);

  int get rating => _rating;

  String get name => _name;

  String get avatarUrl => _avatarUrl;
}
