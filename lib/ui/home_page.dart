import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_map/bloc/bottom_sheet_bloc.dart';
import 'package:flutter_google_map/bloc/event/bottom_sheet_event.dart';
import 'package:flutter_google_map/bloc/event/map_event.dart';
import 'package:flutter_google_map/bloc/map_bloc.dart';
import 'package:flutter_google_map/bloc/state/map_state.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'bottom_sheet.dart';
import 'package:flutter/services.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool bsIsShowing = false;
  static const myLocation = 'myLocation';
  static const myDestination = 'myDestination';

  //google map set up
  Completer<GoogleMapController> _mapController = Completer();
  Map<String, Marker> _markers = <String, Marker>{};
  Set<Polyline> _polylines = Set();
  LocationData _currentLocation;
  Marker currentLocationMarker;
  var location = Location();
  String error;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  _getCurrentLocation() async {
    try {
      _currentLocation = await location.getLocation();
      _onCameraAnimate(_currentLocation);
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      }
      _currentLocation = null;
    }
  }

  _onCameraAnimate(LocationData locationData) async {
    GoogleMapController controller = await _mapController.future;
    setState(() {
      _addMarker(locationData.latitude, locationData.longitude, myLocation);
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(locationData.latitude, locationData.longitude),
          zoom: 16)));
    });
  }

  _addMarker(double lat, double lng, String id) {
    _markers[id] = Marker(
        markerId: MarkerId(id),
        position: LatLng(lat, lng),
        icon: id == myLocation
            ? BitmapDescriptor.defaultMarker
            : BitmapDescriptor.defaultMarkerWithHue(180));
  }

  _moveCameraToFitPolyline(LatLng toLatLng) async {
    if (_markers.values.length > 0) {
      final fromLatLng = _markers[myLocation].position;
      var sLat, sLng, nLat, nLng;
      if (fromLatLng.latitude <= toLatLng.latitude) {
        sLat = fromLatLng.latitude;
        nLat = toLatLng.latitude;
      } else {
        sLat = toLatLng.latitude;
        nLat = fromLatLng.latitude;
      }

      if (fromLatLng.longitude <= toLatLng.longitude) {
        sLng = fromLatLng.longitude;
        nLng = toLatLng.longitude;
      } else {
        sLng = toLatLng.longitude;
        nLng = fromLatLng.longitude;
      }

      LatLngBounds bounds = LatLngBounds(
          northeast: LatLng(nLat, nLng), southwest: LatLng(sLat, sLng));
      final GoogleMapController controller = await _mapController.future;
      try {
        controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 50));
      } on MissingPluginException catch (e) {
        error = e.message;
        print(error);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocBuilder(
        bloc: BlocProvider.of<MapBloc>(context),
        builder: (context, MapState snapshot) {
          if (snapshot is LoadTripInfoState) {
            _addMarker(snapshot.latLng.latitude, snapshot.latLng.longitude,
                myDestination);
            _moveCameraToFitPolyline(snapshot.latLng);
            _polylines.add(Polyline(
                polylineId: PolylineId('polyline'),
                visible: true,
                points: snapshot.route,
                color: Colors.blue));
          }

          if (snapshot is ClearTripInfoState) {
            _markers.remove(myDestination);
            _polylines.clear();
          }
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              onMapCreated: (GoogleMapController controller) {
                _mapController.complete(controller);
              },
              onTap: (latLng) {
                BlocProvider.of<MapBloc>(context).dispatch(
                    OnMapClickEvent(_markers[myLocation].position, latLng));
                if (!bsIsShowing) {
                  showCustomBottomSheet(context);
                  bsIsShowing = true;
                }
                BlocProvider.of<BottomSheetBloc>(context)
                    .dispatch(FindDriverEvent());
              },
              initialCameraPosition: CameraPosition(
                target: LatLng(0, 0),
                zoom: 2,
              ),
              markers: _markers.values.toSet(),
              polylines: _polylines,
            ),
          );
        },
      ),
    );
  }

  void showCustomBottomSheet(BuildContext context) {
    final bottomSheetController = showBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => BottomSheetWidget());
    bottomSheetController.closed.then((value) {
      bsIsShowing = false;
      BlocProvider.of<MapBloc>(context).dispatch(ClearTripInfoEvent());
      BlocProvider.of<BottomSheetBloc>(context).dispatch(ReplanTripEvent());
    });
  }
}
