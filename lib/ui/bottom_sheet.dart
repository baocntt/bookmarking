import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_google_map/bloc/bottom_sheet_bloc.dart';
import 'package:flutter_google_map/bloc/event/bottom_sheet_event.dart';
import 'package:flutter_google_map/bloc/event/map_event.dart';
import 'package:flutter_google_map/bloc/map_bloc.dart';
import 'package:flutter_google_map/bloc/state/bottom_sheet_state.dart';
import 'package:flutter_google_map/model/driver_info.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rating_bar/rating_bar.dart';

class BottomSheetWidget extends StatefulWidget {
  @override
  _BottomSheetWidgetState createState() => _BottomSheetWidgetState();
}

class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  final fromTextController = TextEditingController();
  final toTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.transparent,
      height: 230,
      child: Center(
        child: Container(
          height: 200,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: BlocBuilder(
              bloc: BlocProvider.of<BottomSheetBloc>(context),
              builder: (context, BottomSheetState snapshot) {
                if (snapshot is LoadingState) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (snapshot is LoadedState) {
                  return Center(
                    child: DriverInfoLayout(snapshot.driverInfo),
                  );
                }
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    DecoratedTextField("this is a fixed 'from' location",
                        fromTextController, LocationType.DEPARTURE),
                    DecoratedTextField("this is a fixed 'to' location",
                        toTextController, LocationType.ARRIVAL),
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      color: Colors.grey[800],
                      onPressed: () {
                        BlocProvider.of<BottomSheetBloc>(context)
                            .dispatch(FindDriverEvent());

                        BlocProvider.of<MapBloc>(context).dispatch(
                            OnMapClickEvent(
                                LatLng(16.072637, 108.207063),
                                LatLng(
                                    16.072747119643985, 108.20945799350739)));
                      },
                      child: Text('Find driver',
                          style: TextStyle(color: Colors.white)),
                    )
                  ],
                );
              }),
        ),
      ),
    );
  }
}

class DriverInfoLayout extends StatelessWidget {
  final DriverInfo driverInfo;

  DriverInfoLayout(this.driverInfo);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text(
            'One driver found!!!',
            style:
                TextStyle(color: Colors.black, fontWeight: FontWeight.w300),
          ),
          Image.network(
            driverInfo.avatarUrl,
            height: 50,
            width: 50,
          ),
          Text(
            driverInfo.name,
            style:
                TextStyle(color: Colors.grey[800], fontWeight: FontWeight.bold),
          ),
          RatingBar.readOnly(
            filledIcon: Icons.star,
            emptyIcon: Icons.star_border,
            initialRating: driverInfo.rating.toDouble(),
            maxRating: 5,
            filledColor: Colors.amber,
          ),
          MaterialButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            color: Colors.grey[500],
            onPressed: () {
              BlocProvider.of<BottomSheetBloc>(context)
                  .dispatch(ReplanTripEvent());
              BlocProvider.of<MapBloc>(context).dispatch(ClearTripInfoEvent());
            },
            child:
                Text('Replan journey', style: TextStyle(color: Colors.white)),
          )
        ],
      ),
    );
  }
}

class DecoratedTextField extends StatelessWidget {
  final String textTitle;
  final TextEditingController textController;
  final LocationType locationType;

  DecoratedTextField(this.textTitle, this.textController, this.locationType);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: 15),
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.grey[300], borderRadius: BorderRadius.circular(10)),
      child: TextField(
        enabled: false,
        onSubmitted: (string) {},
        controller: textController,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: textTitle,
            icon: Icon(this.locationType == LocationType.DEPARTURE
                ? Icons.my_location
                : Icons.assistant_photo)),
      ),
    );
  }
}

enum LocationType { DEPARTURE, ARRIVAL }
