import 'dart:async';
import 'package:flutter_google_map/model/step_res.dart';
import 'package:flutter_google_map/model/trip_info_res.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class PlaceService {
  final _apiKey = 'AIzaSyBhDflq5iJrXIcKpeq0IzLQPQpOboX91lY';
  final _googleAPIBaseUrl = 'https://maps.googleapis.com/maps/api';

  Future<dynamic> getStep(
      double fromLat, double fromLng, double tolat, double tolng) async {
    String str_origin =
        "origin=" + fromLat.toString() + "," + fromLng.toString();
    // Destination of route
    String str_dest =
        "destination=" + tolat.toString() + "," + tolng.toString();
    // Sensor enabled
    String sensor = "sensor=false";
    String mode = "mode=driving";
    // Building the parameters to the web service
    String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;
    String url = "$_googleAPIBaseUrl/directions/json"
            "?" +
        parameters +
        "&key=$_apiKey";

    print(url);
    final JsonDecoder _decoder = new JsonDecoder();
    return http.get(url).then((http.Response response) {
      String res = response.body;
      int statusCode = response.statusCode;
//      print("API Response: " + res);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        res = "{\"status\":" +
            statusCode.toString() +
            ",\"message\":\"error\",\"response\":" +
            res +
            "}";
        throw new Exception(res);
      }

      TripInfoRes tripInfoRes;
      try {
        var json = _decoder.convert(res);
        int distance = json["routes"][0]["legs"][0]["distance"]["value"];
        List<StepsRes> steps =
            _parseSteps(json["routes"][0]["legs"][0]["steps"]);

        tripInfoRes = new TripInfoRes(distance, steps);
      } catch (e) {
        throw new Exception(res);
      }

      return tripInfoRes;
    });
  }

  static List<StepsRes> _parseSteps(final responseBody) {
    var list = responseBody
        .map<StepsRes>((json) => new StepsRes.fromJson(json))
        .toList();

    return list;
  }
}
