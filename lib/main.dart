import 'package:flutter/material.dart';
import 'package:flutter_google_map/ui/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/bottom_sheet_bloc.dart';
import 'bloc/map_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //bloc set up
  final mapBloc = MapBloc();
  final bsBloc = BottomSheetBloc();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<MapBloc>(builder: (context) => mapBloc),
        BlocProvider<BottomSheetBloc>(
          builder: (context) => bsBloc,
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    mapBloc.dispose();
    bsBloc.dispose();
  }
}
